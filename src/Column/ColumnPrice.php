<?php

declare(strict_types=1);

namespace SkadminUtils\GridControls\Column;

use SkadminUtils\Utils\Utils\PriceFormat;
use Ublaboo\DataGrid\DataGrid;
use Ublaboo\DataGrid\Row;

use function is_numeric;

class ColumnPrice extends Column
{
    /** @var ?string */
    protected $align = self::AlignRight;

    protected PriceFormat $priceFormat;

    public function __construct(DataGrid $grid, string $key, string $column, string $name, ?PriceFormat $priceFormat = null)
    {
        parent::__construct($grid, $key, $column, $name);

        if ($priceFormat === null) {
            $priceFormat = new PriceFormat();
        }

        $this->priceFormat = $priceFormat;
    }

    public function getColumnValue(Row $row): mixed
    {
        $value = parent::getColumnValue($row);

        if (! is_numeric($value)) {
            return $value;
        }

        return $this->priceFormat->toString($value);
    }

    public function getPriceFormat(): ?PriceFormat
    {
        return $this->priceFormat;
    }

    public function setPriceFormat(?PriceFormat $priceFormat): ColumnPrice
    {
        $this->priceFormat = $priceFormat;

        return $this;
    }
}

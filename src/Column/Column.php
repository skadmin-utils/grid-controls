<?php

declare(strict_types=1);

namespace SkadminUtils\GridControls\Column;

use Ublaboo\DataGrid\Column\Column as UblabooColumn;

class Column extends UblabooColumn
{
    public const AlignLeft   = 'left';
    public const AlignCenter = 'center';
    public const AlignRight  = 'right';
}

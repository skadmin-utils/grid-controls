<?php

declare(strict_types=1);

namespace SkadminUtils\GridControls\UI;

use App\AdminModule\Presenters\CoreAdminPresenter;
use App\CoreModule\Presenters\CorePresenter;
use DateTime;
use InvalidArgumentException;
use Nette\Forms\Container;
use Nette\Forms\Controls\SelectBox;
use Nette\Forms\IControl;
use Skadmin\Seo\Utils\UtilsSeoRenderSeoHref;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\GridControls\UI\Traits\TGridColumn;
use Ublaboo\DataGrid\DataGrid;
use Ublaboo\DataGrid\Filter\FilterSelect;
use Ublaboo\DataGrid\Filter\FilterText;
use Ublaboo\DataGrid\Filter\IFilterDate;
use Ublaboo\DataGrid\InlineEdit\InlineAdd;
use Ublaboo\DataGrid\InlineEdit\InlineEdit;
use UnexpectedValueException;

use function assert;
use function defined;
use function file_exists;
use function str_replace;

class GridDoctrine extends DataGrid
{
    use TGridColumn;

    /** @var Translator|null */
    protected $translator = null;
    protected CorePresenter $presenter;

    public function __construct(CorePresenter $presenter)
    {
        $this->presenter = $presenter;
        $this->translator = $presenter->translator;
        $this->setStrictSessionFilterValues(false); // pokud záznam v session neexistuje, tak se ignoruje

        parent::__construct();
        $this->setTemplateFile($this->getModifyTemplate(__DIR__ . '/templates/ublaboo-datagrid.latte'));
        $this->setCustomPaginatorTemplate($this->getModifyTemplate(__DIR__ . '/templates/ublaboo-datagrid-paginator.latte'));
        $this->setItemsPerPageList([10, 15, 20, 30, 40, 50]);

        $this::$iconPrefix = 'fas fa-';
    }

    /**
     * @param string $treeViewHasChildrenColumn
     */
    public function setTreeView(callable $getChildrenCallback, $treeViewHasChildrenColumn = 'has_children'): DataGrid
    {
        parent::setTreeView($getChildrenCallback, $treeViewHasChildrenColumn);
        $this->setTemplateFile($this->getModifyTemplate(__DIR__ . '/templates/ublaboo-datagrid-tree.latte'));

        return $this;
    }

    /**
     * @param string[]|array|string $columns
     */
    public function addFilterText(string $key, string $name, $columns = null): FilterText
    {
        $filter = parent::addFilterText($key, $name, $columns);
        $filter->setTemplate($this->getModifyTemplate(__DIR__ . '/templates/ublaboo-datagrid-filter-text-select.latte'));

        return $filter;
    }

    /**
     * @param string[] $options
     */
    public function addFilterSelect(string $key, string $name, array $options, ?string $column = null): FilterSelect
    {
        $filter = parent::addFilterSelect($key, $name, $options, $column);
        $filter->setTemplate($this->getModifyTemplate(__DIR__ . '/templates/ublaboo-datagrid-filter-text-select.latte'));

        return $filter;
    }

    public function render(): void
    {
        if ($this->getInlineAdd() !== null) {
            $this->getInlineAdd()->setClass('btn btn-xs btn-primary ajax');
        }

        if ($this->getInlineEdit() !== null) {
            $this->getInlineEdit()->setClass('btn btn-xs btn-primary ajax');
        }

        if ($this->getItemsDetail() !== null) {
            $this->getItemsDetail()->setClass('btn btn-xs btn-outline-primary ajax');
        }

        parent::render();
    }

    public function renderSkip(): void
    {
        parent::render();
    }

    /**
     * @param mixed[] $values
     */
    public function setFilterContainerDefaults(Container $container, array $values, ?string $parentKey = null): void
    {
        foreach ($container->getComponents() as $key => $control) {
            if (!isset($values[$key])) {
                continue;
            }

            if ($control instanceof Container) {
                $this->setFilterContainerDefaults($control, (array)$values[$key], (string)$key);

                continue;
            }

            $value = $values[$key];

            if ($value instanceof DateTime) {
                if ($parentKey !== null) {
                    $filter = $this->getFilter($parentKey);
                } else {
                    $filter = $this->getFilter((string)$key);
                }

                if ($filter instanceof IFilterDate) {
                    $value = $value->format($filter->getPhpFormat());
                }
            }

            try {
                if (!$control instanceof IControl) {
                    throw new UnexpectedValueException();
                }

                if ($control instanceof SelectBox) {
                    if (isset($control->getItems()[$value])) {
                        $control->setValue($value);
                    }
                } else {
                    $control->setValue($value);
                }
            } catch (InvalidArgumentException $e) {
                if ($this->strictSessionFilterValues) {
                    throw $e;
                }
            }
        }
    }

    public function getInlineAddPure(): InlineAdd
    {
        $inlineAdd = parent::getInlineAdd();
        assert($inlineAdd instanceof InlineAdd);

        return $inlineAdd;
    }

    public function getInlineEditPure(): InlineEdit
    {
        $inlineEdit = parent::getInlineEdit();
        assert($inlineEdit instanceof InlineEdit);

        return $inlineEdit;
    }

    public function addToolbarSeo(string $type, SimpleTranslation|string $name): void
    {
        $this->toolbarButtons['toolbar-seo'] = UtilsSeoRenderSeoHref::createDataGridSeoToolbar($this, $type, $this->translator->translate($name));
    }

    public function getModifyTemplate(string $template): string
    {
        if (defined(CoreAdminPresenter::class . '::VersionBS')) {
            $resultTemplate = str_replace('.latte', '-bs' . CoreAdminPresenter::VersionBS . '.latte', $template);
        } else {
            $resultTemplate = $template;
        }

        if (!file_exists($resultTemplate)) {
            $resultTemplate = $template;
        }

        return $resultTemplate;
    }
}

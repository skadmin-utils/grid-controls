<?php

declare(strict_types=1);

namespace SkadminUtils\GridControls\UI;

use App\Components\Control;
use App\CoreModule\Presenters\CorePresenter;
use Nette\Security\User;
use Nette\SmartObject;
use Skadmin\Translator\Translator;

class GridControl extends Control
{
    use SmartObject;

    /** @var callable[] */
    public array            $onFlashmessage;
    protected Translator    $translator;
    protected User          $loggedUser;
    protected CorePresenter $presenter;

    public function __construct(Translator $translator, User $user)
    {
        $this->translator = $translator;
        $this->loggedUser = $user;
    }

    protected function isAllowed(string $resource, string $privilege): bool
    {
        return $this->loggedUser->isAllowed($resource, $privilege);
    }
}

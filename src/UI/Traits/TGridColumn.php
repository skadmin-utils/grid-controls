<?php

declare(strict_types=1);

namespace SkadminUtils\GridControls\UI\Traits;

use SkadminUtils\GridControls\Column\ColumnPrice;
use SkadminUtils\Utils\Utils\PriceFormat;

trait TGridColumn
{
    public function addColumnPrice(string $key, string $name, ?string $column = null, ?PriceFormat $priceFormat = null): ColumnPrice
    {
        $column ??= $key;

        $columnText = new ColumnPrice($this, $key, $column, $name, $priceFormat);
        $this->addColumn($key, $columnText);

        return $columnText;
    }
}
